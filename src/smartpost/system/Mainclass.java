/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost.system;
 
import com.sun.javaws.Main;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.ListCell;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
 
/**
 *
 * @author Ratio
 */
public class Mainclass {
    static public Mainclass mc=null;
    private XMLtoSmartPost xml=XMLtoSmartPost.getInstance();
    private ArrayList<Object> objects = new ArrayList<Object>();
   
    private Mainclass(){
        xml.getSmartposts();
    }
   
    public static Mainclass getInstance(){
        if (mc==null){
           mc=new Mainclass();
        }      
        return mc;
    }
    
    
    public ArrayList getCoordinates(String s,String d){
        ArrayList<Smartpost> smartposts = xml.getSmartpostList();
        String slat=null;
        String slong=null;
        String dlat=null;
        String dlong=null;
        int i;
        for(i=0;i<xml.getSmartPostListSize();i++){
            if((s.equals(xml.getSmartpost(i).getPostoffice()))){
                slong=xml.getSmartpost(i).getLongitude();
                slat=xml.getSmartpost(i).getLatitude();  
            }
            if((d.equals(xml.getSmartpost(i).getPostoffice()))){
                dlat=xml.getSmartpost(i).getLatitude();
                dlong=xml.getSmartpost(i).getLongitude();
            }
 
        }

        ArrayList<String> coordinateList = new ArrayList<String>();
        coordinateList.add(slat);
        coordinateList.add(slong);
        coordinateList.add(dlat);
        coordinateList.add(dlong);
        return coordinateList;
        
    }
    
   
    
    
    public int getPostClass(Package p){
        if(p.getClass().getSimpleName().equals("firstClass")){
            return 1;
        }
        if(p.getClass().getSimpleName().equals("secondClass")){
            return 2;
        }
        if(p.getClass().getSimpleName().equals("thirdClass")){
            return 3;
        }
        return 0;
    }
   
 
   
    public ArrayList<Object> getDefaultObjectsList() { //create default objects
        objects.clear();
        DVD ob1 = new DVD("DVD", "0.1", "10", "15", "3", "Ei särkyvää");
        Glass ob2 = new Glass("Lasi", "0.2", "5", "10", "5", "Särkyvää");
        Shelf ob3 = new Shelf("Hylly", "10", "100", "150", "50", "Ei särkyvää");
        Laptop ob4 = new Laptop("Läppäri", "2", "40", "30", "4", "Ei särkyvää");
        objects.add(ob1);
        objects.add(ob2);
        objects.add(ob3);
        objects.add(ob4);

        return objects;
    }

}

