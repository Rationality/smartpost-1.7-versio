/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost.system;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 * FXML Controller class
 *
 * @author Ratio
 */
public class FXMLmusicDocumentController implements Initializable {
    @FXML
    private Button playMusicButton;
    @FXML
    private Button stopMusicButton;
    
    final URL resource = getClass().getResource("Hyvaa Joulua v2.wav");
    final Media media = new Media(resource.toString());
    final MediaPlayer mediaPlayer = new MediaPlayer(media);

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    @FXML
    private void playMusicAction(ActionEvent event) { 
        mediaPlayer.play();
    
    }   

    @FXML
    private void stopMusicAction(ActionEvent event) {
        mediaPlayer.stop();
    }
    
}
