/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost.system;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;

/**
 * FXML Controller class
 *
 * @author Ratio
 */
public class FXMLStorageDocumentController implements Initializable {
    @FXML
    private ListView<String> storageListView;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Storage storage = Storage.getInstance();
        ArrayList<Package> packages = storage.getPackageList();
        for(Package p : packages){
            storageListView.getItems().add(p.name+", "+p.getClass().getSimpleName()
                    +", "+p.getSendingMachine()+" -> "+p.getDestinationMachine());
        }
        
        
    }    
    
}
